'use strict';
import * as THREE from 'three';
import { OBJLoader2 } from 'three/examples/jsm/loaders/OBJLoader2.js';
import myVertexShader from '../shaders/lion_vertex_shader.js';
import myFragmentShader from '../shaders/lion_fragment_shader.js';
import myParticleVertexShader from '../shaders/particle_lion_vertex_shader.js';
import myParticleFragmentShader from '../shaders/particle_lion_fragment_shader.js';

export default class Lion {
  constructor() {
    this.uniforms = {
      mouseposition:{ type: 'v2', value: new THREE.Vector2(0., 0.)},
      chaos: { type: 'f', value: 0 }
    };
        
    this.material = new THREE.ShaderMaterial({
        uniforms: this.uniforms,
        vertexShader: myVertexShader,
        fragmentShader:  myFragmentShader,
        wireframe: false,
        transparent: true
    });

    this.particlematerial = new THREE.ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader: myParticleVertexShader,
      fragmentShader:  myParticleFragmentShader,
      wireframe: false,
      transparent: true
  });
  }


  async load() {
    let promise = new Promise((resolve, reject) => {
      var loader = new OBJLoader2();
      loader.load( 'mylion.obj', object => {
        object.children.forEach( obj => {
          obj.material = this.material;
        });
        this.mesh = object;

        this.meshpoints = new THREE.Points(this.mesh.children[0].geometry, this.particlematerial);

        resolve(this.mesh);
      }, undefined, error => {
        console.error( error );
        reject(error);
      } );
    });
    return await promise;
  }
}