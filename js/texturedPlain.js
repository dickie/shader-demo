'use strict';
import * as THREE from 'three';
import myVertexShader from '../shaders/trail_vertex_shader.js';
import myFragmentShader from '../shaders/trail_fragment_shader.js';

export default class TexturedPlain {
  constructor(width, height, inputTexture) {
    this.inputTexture = inputTexture;
    this.width = width;
    this.height = height;
    
    this.material = new THREE.ShaderMaterial({
        uniforms: { 
            inputTexture: {type: 't',   value: this.inputTexture },
        },
        vertexShader: myVertexShader,
        fragmentShader:  myFragmentShader,
        wireframe: false,
        transparent: true
    });
    
    this.geometry = new THREE.PlaneGeometry( this.width, this.height, 1, 1);
    this.mesh = new THREE.Mesh( this.geometry, this.material );
  }

  setInputTexture(inputTexture) {
    this.material.uniforms.inputTexture.value = inputTexture;
  }
}