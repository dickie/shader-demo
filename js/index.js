import * as THREE from 'three';
import TOC from 'three-orbit-controls';
import Lion from './lion';
import TrailCalc from './trailCalc';
import TexturedPlain from './texturedPlain';

//canvas
var canvas = document.getElementById('main');

//renderer
var renderer = new THREE.WebGLRenderer({canvas: canvas, alpha: false});
renderer.setClearColor( new THREE.Color(0x000000), 1 );
renderer.setSize(canvas.width, canvas.height);

//scenes
var scene = new THREE.Scene();
var resultScene = new THREE.Scene();

//camera
var camera = new THREE.PerspectiveCamera(49, renderer.context.canvas.width / renderer.context.canvas.height, 0.1, 1000);
//var camera = new THREE.OrthographicCamera( canvas.width / - 2, canvas.width / 2, canvas.height / 2, canvas.height / - 2, 1, 1000 );
camera.position.set(0,0,800);


var OrbitControls = TOC(THREE);
var controls = new OrbitControls(camera, canvas)

//lights
scene.add( new THREE.AmbientLight( 0x404040 ) );
scene.add( new THREE.DirectionalLight( 0xffffff, 0.9 ) );

const lion = new Lion();
lion.load().then(() => {
  scene.add(lion.meshpoints);
  resultScene.add(lion.mesh);
});

const outputTextureA =  new THREE.WebGLRenderTarget( canvas.width, canvas.height, 
  { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter});
const outputTextureB =  new THREE.WebGLRenderTarget( canvas.width, canvas.height, 
  { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter});
var outputTexture = outputTextureA;
const trailCalc = new TrailCalc(renderer, null);
var endResult = new TexturedPlain(canvas.width, canvas.height, null);

resultScene.add(endResult.mesh);

let chaos = 0;
let chaosdelta = 0.0;

function renderScene() {
  controls.update();
  chaos += chaosdelta;

  if (chaos > 300 && chaosdelta > 0) {
    chaosdelta = -1;
  } else if (chaos < 0 && chaosdelta < 0) {
    chaosdelta = 0;
  } else if (chaosdelta > 0 && chaos < 300) {
    chaosdelta = Math.min(10, chaosdelta + 0.02);
  }
  lion.material.uniforms.chaos.value = chaos;

  outputTexture =  outputTexture ===  outputTextureA ? outputTextureB :  outputTextureA;
  renderer.setRenderTarget(outputTexture);
  renderer.render(scene, camera);

  if (chaosdelta!= 0){
    trailCalc.setInputTexture(outputTexture ===  outputTextureA ? outputTextureB :  outputTextureA);
  } else {
    trailCalc.setInputTexture(null);
  }
  
  trailCalc.setPreviousOutputTexture(trailCalc.outputTexture);
  trailCalc.calc();

  //Just show the end result on a plain
  endResult.setInputTexture(trailCalc.outputTexture);
  renderer.setRenderTarget(null);
  renderer.render(resultScene, camera);
}

document.getElementById('play-button').onclick = () => {
  chaosdelta = 0.02;
};

window.onmousemove = (e) => {
  lion.material.uniforms.mouseposition.value = 
    new THREE.Vector2(e.x - canvas.offsetLeft - canvas.width/2, -e.y  - canvas.offsetTop + canvas.height/2);
}

//better then  timout
window.requestAnimFrame = (() => {
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 30);
          };
})();

(function animloop() {
  requestAnimFrame(animloop);
  renderScene();
})();