'use strict';
import * as THREE from 'three';
import OffScreenRenderer from './offScreenRenderer';
var myVertexShader = require("../shaders/trail_vertex_shader.js");
var myFragmentShader = require("../shaders/trail_fragment_shader.js");

export default class TrailCalc extends OffScreenRenderer {
  constructor(renderer, inputTexture) {
    super(renderer, inputTexture, myVertexShader, myFragmentShader);
    this.material.uniforms.previousOutputTexture = { type: 't',   value: inputTexture };
    this.material.uniforms.advectionTexture = { type: 't',   
      value: THREE.ImageUtils.loadTexture( 'textures/advection.png' ) };
  }

  setPreviousOutputTexture(previousOutputTexture) {
    this.material.uniforms.previousOutputTexture.value = previousOutputTexture;
  }
}