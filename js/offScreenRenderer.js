'use strict';
import * as THREE from 'three';

export default class OffScreenRenderer {
  constructor(renderer, inputTexture, vertextShader, fragmentShader, widthSegments = 200, heightSegments = 200) {
    this.inputTexture = inputTexture;
    this.renderer = renderer;
    this.width = renderer.context.canvas.width;
    this.height = renderer.context.canvas.height;
    this.scene = new THREE.Scene();
    this.camera = new THREE.OrthographicCamera(this.width / - 2, this.width / 2, this.height / 2, this.height / - 2, 0.1, 1000);
    this.camera.position.set(0,0,100);
    this.outputTextureA =  new THREE.WebGLRenderTarget( this.width, this.height, 
      { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter});
    this.outputTextureB =  new THREE.WebGLRenderTarget( this.width, this.height, 
      { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter});
    this.outputTexture =  this.outputTextureA;  
    this.uniforms = { 
        inputTexture: {type: 't',   value: this.inputTexture },
    };
    this.material = new THREE.ShaderMaterial({
        uniforms: this.uniforms,
        vertexShader: vertextShader,
        fragmentShader:  fragmentShader,
        wireframe: false,
        transparent: false
    });
    
    this.geometry = new THREE.PlaneGeometry( this.width, this.height, widthSegments, heightSegments);
    this.mesh = new THREE.Mesh( this.geometry, this.material );
    this.scene.add(this.mesh);
  }

  setInputTexture(inputTexture) {
    this.inputTexture = inputTexture;
    this.material.uniforms.inputTexture.value = this.inputTexture;
  }

  getOutputTexture() {
    return this.outputTexture ==  this.outputTextureA ?  this.outputTextureB :  this.outputTextureA;
  }

  calc() {
    this.outputTexture =  this.outputTexture ==  this.outputTextureA ?  this.outputTextureB :  this.outputTextureA;
    this.renderer.setRenderTarget(this.outputTexture);
    this.renderer.render(this.scene, this.camera);
  }
}