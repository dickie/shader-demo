module.exports = `
uniform vec2 mouseposition;
uniform float chaos;
varying vec4 vColor;

    
void main(){
  gl_PointSize = 2.;
  vColor = vec4(247./255., 115./255., 5./255., 0.91);

  vec2 mouseToPiont = position.xy - mouseposition;
  float distance = length(mouseToPiont);
  
  if (distance < 20.) {
    gl_PointSize = 5.;
    vColor = vec4(1., 1., 1., 0.1);
  }

  vec3 newPosition = position;
  if (chaos > 1.) {
    float angle = chaos*newPosition.y/800.;
    newPosition.z = sin(angle) * newPosition.x;
    newPosition.x *= cos(angle);
  }

  gl_Position = projectionMatrix * modelViewMatrix * vec4(newPosition, 1.0);
}`;