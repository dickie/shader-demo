module.exports = `
uniform float chaos;
varying vec3 vPosition;
varying vec3 vNormal;

const vec3 lightPos = vec3(100., .0, 100.);
const vec4 color = vec4(247./255., 115./255., 5./255., 1.);

void main() {
    
  if (chaos <= 0.) {
    vec3 lightDir = normalize(lightPos - vPosition);
    float lambertian = max(dot(lightDir, vNormal), 0.3);
    gl_FragColor = vec4(lambertian*color.rgb, 1.0);
  } else {
    discard;
  }
}`;