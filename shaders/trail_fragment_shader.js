module.exports = `
uniform sampler2D inputTexture;
uniform sampler2D previousOutputTexture;
uniform sampler2D advectionTexture;
varying vec2 vUv;

void main() {
  vec4 color = texture2D(inputTexture, vUv);
  if (color.r > .0) {
    // the particle
    gl_FragColor = color;
  } else {
    // trail

    //move up
    vec2 advecColor = texture2D(advectionTexture, vUv).rg;
    vec2 velocity = (advecColor - vec2(.5, .5))/200.;
    velocity.y = velocity.y + 0.0015;
    vec4 previousColor = texture2D(previousOutputTexture, vUv-velocity);

    //desolve:
    previousColor.a = previousColor.a*0.995;

    //change color
    //previousColor.b = previousColor.b * 1.01;

    gl_FragColor = previousColor;
  }
}`;