module.exports = `
uniform float chaos;
varying vec4 vColor;

void main() {
  if (chaos <= 0.) {
    discard;
  } else {
    gl_FragColor = vColor;
  }
}`;